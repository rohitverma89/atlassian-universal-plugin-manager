package com.atlassian.upm.test;

import java.net.URI;

public enum TestPlugins
{
    BUNDLED("test-plugin-v2-bundled", null, "Test Plugin v2 (bundled)"),
    UPGRADABLE_BUNDLED("test-plugin-v2-bundled-upgradable", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-bundled-upgradable-latest-update.jar"), "Test Plugin v2 (upgradable bundled)"),
    SYSTEM("test-plugin-v2-system", null, "Test Plugin v2 (system)"),
    STATIC("test-plugin-v1-classpath", null, "Test Plugin v1 (classpath)"),
    INSTALLABLE("test-plugin-v2-installable", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-installable.jar"), "Test Plugin v2 (installable)"),
    INSTALLABLE_WITH_CONFIG("test-plugin-v2-installable-with-config", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-installable-with-config.jar"), "Test Plugin v2 (installable with config)"),
    INSTALLABLE_REQUIRES_RESTART("test-plugin-v2-installable-requires-restart", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-installable-requires-restart.jar"), "Test Plugin v2 (installable, requires restart)"),
    BUNDLED_WITH_CONFIG("test-plugin-v2-bundled-with-config", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-bundled-with-config.jar"), "Test Plugin v2 (bundled with config)"),
    NO_BINARY_URL("test-plugin-v2-no-binary-url", null, "No Binary Url Test Plugin"),
    OBR("com.atlassian.upm.atlassian-universal-plugin-manager-test-plugin-v2-obr", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-obr.obr"), "Installable Test OBR Plugin"),
    OBR_DEP("com.springsource.org.antlr-3.1.3", null, "ANTLR"),
    BROKEN_UPGRADE_V1("test-plugin-v2-broken-upgrade", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-broken-upgrade-1.0.jar"), "Test Plugin v2 (broken-upgrade)"),
    NONEXISTENT_JAR("does_not_exist", buildFakePacDownloadUri("does_not_exist.jar"), null),
    NONDEPLOYABLE("non-deployable-test-plugin", buildFakePacDownloadUri("non-deployable-test-plugin.jar"), "Non-deployable test plugin"),
    NOT_RELOADABLE("test-plugin-v2-not-reloadable", null, "Test Plugin v2 (not reloadable)"),
    USER_INSTALLED_WITH_MODULES("test-plugin-v2-user-installed-with-modules", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-user-installed-with-modules.jar"), "User installed test plugin with modules"),
    USER_INSTALLED_FLAGGED_AS_SYSTEM_REQUIRED("test-plugin-v2-user-installed-flagged-as-system-required", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-user-installed-flagged-as-system-required.jar"), "This is a test plugin (user installed, flagged as system, required)"),
    NOT_ENABLED_BY_DEFAULT("test-plugin-v2-not-enabled-by-default", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-not-enabled-by-default.jar"), "Test Plugin v2 (not enabled by default)"),
    UPGRADEABLE_REQUIRES_RESTART_V1("test-plugin-v2-upgradeable-requires-restart", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-upgradeable-requires-restart-1.0.jar"), "Test Plugin v2 (upgradeable, requires restart, v1.0)"),
    UPGRADEABLE_REQUIRES_RESTART_V2("test-plugin-v2-upgradeable-requires-restart", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-upgradeable-requires-restart-1.0.jar"), "Test Plugin v2 (upgradeable, requires restart, v1.0)"),
    LEGACY_PLUGIN("test-plugin-v1-installable", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v1-installable.jar"), "Test Plugin v1 (installable)"),
    UNSUPPORTED_PLUGIN("test-plugin-v99-unsupported", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v99-unsupported.jar"), "Test Plugin v99 (unsupported)"),
    UPM("com.atlassian.upm.atlassian-universal-plugin-manager-plugin", buildFakePacDownloadUri("com.atlassian.upm.atlassian-universal-plugin-manager-plugin"), "Atlassian Universal Plugin Manager Plugin"),
    JIRA_CALENDAR("com.atlassian.jira.ext.calendar", null, "JIRA Calendar Plugin"),
    JIRA_CHART("com.atlassian.jira.ext.charting", null, "JIRA Charting Plugin"),
    ALREADY_INSTALLED("com.atlassian.upm.atlassian-universal-plugin-manager-plugin", null, "Already installed plugin - UPM", true),
    CANNOT_DISABLE_MODULE("test-plugin-v2-cannot-disable-module", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-cannot-disable-module.jar"), "Test Plugin v2 (cannot disable module)"),
    XML_PLUGIN("test-plugin-v2-xml-plugin-artifact", buildFakePacXmlPluginDownloadUri("plugin-v2-xml-plugin-artifact.xml"), "Test Plugin v2 (xml plugin artifact)"),
    REFIMPL_SPI_PLUGIN("com.atlassian.upm.refimpl-spi-plugin", null, "refimpl-spi-plugin"),
    OSGI_BUNDLE("com.atlassian.upm.atlassian-universal-plugin-manager-test-osgi-bundle-1.0", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-osgi-bundle.jar"), "Test OSGi Bundle"),
    UNRECOGNISED_MODULE_TYPE("test-plugin-v2-unrecognised-module-type", buildFakePacDownloadUri("atlassian-universal-plugin-manager-test-plugin-v2-unrecognised-module-type.jar"), "Unrecognised-module-type");
 
    private final String key;
    private URI downloadUri;
    private String name;
    private boolean installed;

    TestPlugins(String key, URI downloadUri, String name)
    {
        this.key = key;
        this.downloadUri = downloadUri;
        this.name = name;
        this.installed = false;
    }

    TestPlugins(String key, URI downloadUri, String name, boolean installed)
    {
        this.key = key;
        this.downloadUri = downloadUri;
        this.name = name;
        this.installed = installed;
    }

    public String getKey()
    {
        return key;
    }

    public String getEscapedKey()
    {
        return key + "-key";
    }

    public String getName()
    {
        return name;
    }

    public URI getDownloadUri(URI baseUri)
    {
        if (downloadUri == null)
        {
            throw new UnsupportedOperationException(key + " has no downloadable artifact");
        }
        return URI.create(baseUri.toASCIIString() + "/" + downloadUri.toASCIIString()).normalize();
    }

    public boolean isInstalled()
    {
        return installed;
    }

    private static URI buildFakePacDownloadUri(String fileName)
    {
        return URI.create("rest/fakepac/1.0/plugins/" + fileName);
    }

    private static URI buildFakePacXmlPluginDownloadUri(String plugin)
    {
        return URI.create("rest/fakepac/1.0/xmlplugins/" + plugin);
    }
}
