package com.atlassian.upm.refimpl.spi.rest.resources;

import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.atlassian.upm.refimpl.spi.RefimplPermissionService;
import com.atlassian.upm.spi.Permission;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.PRECONDITION_FAILED;

/**
 * Provides a REST resource for adding and removing permissions to the test {@code PermissionService}.
 */
@Path("/")
public class RefimplPermissionServiceResource
{
    private final RefimplPermissionService permissionService;

    public RefimplPermissionServiceResource(RefimplPermissionService permissionService)
    {
        this.permissionService = checkNotNull(permissionService, "permissionService");
    }

    @POST
    @Path("{permissionKey}")
    public Response add(@PathParam("permissionKey") String permissionKey)
    {
        boolean success = permissionService.addPermission(Permission.valueOf(permissionKey));
        if (success)
        {
            return Response.ok().build();
        }
        else
        {
            return Response.status(PRECONDITION_FAILED).build();
        }
    }

    @POST
    @Path("all")
    public Response addAll()
    {
        for (Permission permission : Permission.values())
        {
            permissionService.addPermission(permission);
        }
        return Response.ok().build();
    }

    @POST
    @Path("reset")
    public Response reset()
    {
        permissionService.resetPermissions();
        permissionService.resetModulePermissions();
        permissionService.resetPluginPermissions();
        return Response.ok().build();
    }

    @DELETE
    @Path("{permissionKey}")
    public Response remove(@PathParam("permissionKey") String permissionKey)
    {
        boolean success = permissionService.removePermission(Permission.valueOf(permissionKey));
        if (success)
        {
            return Response.ok().build();
        }
        else
        {
            return Response.status(PRECONDITION_FAILED).build();
        }
    }

    @DELETE
    @Path("all")
    public Response removeAll()
    {
        for (Permission permission : Permission.values())
        {
            permissionService.removePermission(permission);
        }
        return Response.ok().build();
    }

    @POST
    @Path("plugin/{pluginKey}")
    public Response blacklistPluginPermissions(@PathParam("pluginKey") String pluginKey)
    {
        boolean success = permissionService.blacklistPluginPermissions(pluginKey);
        if (success)
        {
            return Response.ok().build();
        }
        else
        {
            return Response.status(PRECONDITION_FAILED).build();
        }
    }

    @DELETE
    @Path("plugin/{pluginKey}")
    public Response unblacklistPluginPermissions(@PathParam("pluginKey") String pluginKey)
    {
        boolean success = permissionService.unblacklistPluginPermissions(pluginKey);
        if (success)
        {
            return Response.ok().build();
        }
        else
        {
            return Response.status(PRECONDITION_FAILED).build();
        }
    }

    @POST
    @Path("module/{moduleKey}")
    public Response blacklistModulePermissions(@PathParam("moduleKey") String moduleKey)
    {
        boolean success = permissionService.blacklistModulePermissions(moduleKey);
        if (success)
        {
            return Response.ok().build();
        }
        else
        {
            return Response.status(PRECONDITION_FAILED).build();
        }
    }

    @DELETE
    @Path("module/{moduleKey}")
    public Response unblacklistModulePermissions(@PathParam("moduleKey") String moduleKey)
    {
        boolean success = permissionService.unblacklistModulePermissions(moduleKey);
        if (success)
        {
            return Response.ok().build();
        }
        else
        {
            return Response.status(PRECONDITION_FAILED).build();
        }
    }

}
