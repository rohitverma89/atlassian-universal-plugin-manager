package com.atlassian.upm.refimpl.spi;

import java.util.concurrent.CopyOnWriteArraySet;

import com.atlassian.upm.spi.Permission;
import com.atlassian.upm.spi.PermissionService;
import com.atlassian.upm.spi.Plugin;

import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_ENABLEMENT;

/**
 * A permission service implementation that dynamically allows for permissions to be added or removed.
 * By default, the ability to manage plugin enablement is the only permission.
 * <p/>
 * This overrides the default permission service provided with the Universal Plugin Manager.
 */
public class RefimplPermissionService implements PermissionService
{
    private CopyOnWriteArraySet<Permission> permissions;
    private CopyOnWriteArraySet<String> blacklistedModules;
    private CopyOnWriteArraySet<String> blacklistedPlugins;

    public RefimplPermissionService()
    {
        permissions = new CopyOnWriteArraySet<Permission>();
        blacklistedModules = new CopyOnWriteArraySet<String>();
        blacklistedPlugins = new CopyOnWriteArraySet<String>();
        resetPermissions();
    }

    public boolean hasPermission(String username, Permission permission)
    {
        if (permissions.contains(permission))
        {
            return true;
        }
        return false;
    }

    public boolean hasPermission(final String username, final Permission permission, final Plugin.Module module)
    {
        if (module != null && blacklistedModules.contains(module.getCompleteKey()))
        {
            return false;
        }
        return hasPermission(username, permission);
    }

    public boolean hasPermission(final String username, final Permission permission, final Plugin plugin)
    {
        if (plugin != null && blacklistedPlugins.contains(plugin.getKey()))
        {
            return false;
        }
        return hasPermission(username, permission);
    }

    /**
     * Adds the specified {@code Permission}. Returns true if added, false if not (because it already existed).
     *
     * @param permission permission to add
     * @return Returns true if added, false if not (because it already existed).
     */
    public boolean addPermission(Permission permission)
    {
        return permissions.add(permission);
    }

    /**
     * Removes the specified {@code Permission}. Returns true if removed, false if not (because it did not previously exist).
     *
     * @param permission permission to remove
     * @return Returns true if removed, false if not (because it did not previously exist).
     */
    public boolean removePermission(Permission permission)
    {
        return permissions.remove(permission);
    }

    /**
     * Flags a module key that should have its permission checks return false.
     *
     * @param moduleCompleteKey identifies the module
     * @return true if added, false if not
     */
    public boolean blacklistModulePermissions(String moduleCompleteKey)
    {
        return blacklistedModules.add(moduleCompleteKey);
    }

    /**
     * Unflags a module key so that the permission check will behave as normal.
     *
     * @param moduleCompleteKey identifies the module
     * @return true if removed, false if not
     */
    public boolean unblacklistModulePermissions(String moduleCompleteKey)
    {
        return blacklistedModules.remove(moduleCompleteKey);
    }

    /**
     * Flags a plugin key that should have its permission checks return false.
     *
     * @param pluginKey identifies the plugin
     * @return true if added, false if not
     */
    public boolean blacklistPluginPermissions(String pluginKey)
    {
        return blacklistedPlugins.add(pluginKey);
    }


    /**
     * Unflags a plugin key so that its permission checks will behave as normal.
     *
     * @param pluginKey identifies the plugin
     * @return true if removed, false if not
     */
    public boolean unblacklistPluginPermissions(String pluginKey)
    {
        return blacklistedPlugins.remove(pluginKey);
    }

    /**
     * Resets the {@code Permission}s to the default (so that plugin enablement is the only allowed permission).
     */
    public void resetPermissions()
    {
        permissions.clear();
        permissions.add(MANAGE_PLUGIN_ENABLEMENT);
    }

    /**
     * Unflags all modules from having their permissions subverted.
     */
    public void resetModulePermissions()
    {
        blacklistedModules.clear();
    }

    /**
     * Unflags all plugins from having their permissions subverted.
     */
    public void resetPluginPermissions()
    {
        blacklistedPlugins.clear();
    }
}
