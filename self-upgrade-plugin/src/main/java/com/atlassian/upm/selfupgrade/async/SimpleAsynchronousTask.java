package com.atlassian.upm.selfupgrade.async;

import java.net.URI;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;

import com.atlassian.upm.selfupgrade.rest.representations.TaskRepresentation;

import com.google.common.collect.ImmutableMap;

public class SimpleAsynchronousTask implements Runnable
{
    private final Callable<TaskStatus> handler;
    private final int pollDelay;
    private final Date timestamp;
    private final String type;
    private final URI uri;
    private final String username;
    private final String source;
    private final AtomicReference<TaskStatus> statusHolder;
    
    public SimpleAsynchronousTask(Callable<TaskStatus> handler, String type, String source, int pollDelay, URI uri, String username)
    {
        this.handler = handler;
        this.statusHolder = new AtomicReference<TaskStatus>(TaskStatus.inProgress(source));
        this.source = source;
        this.pollDelay = pollDelay;
        this.timestamp = new Date();
        this.type = type;
        this.uri = uri;
        this.username = username;
    }
    
    public void run()
    {
        try
        {
            TaskStatus result = handler.call();
            statusHolder.set(result);
        }
        catch (Exception e)
        {
            statusHolder.set(TaskStatus.error(e.toString(), source));
        }
    }
    
    public TaskRepresentation getRepresentation()
    {
        TaskStatus status = statusHolder.get(); 
        return new TaskRepresentation(type,
                                      status.isDone() ? null : pollDelay,
                                      status.getRepresentation(),
                                      ImmutableMap.of("self", uri),
                                      timestamp,
                                      username);
    }
}
