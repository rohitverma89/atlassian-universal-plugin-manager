package it.com.atlassian.upm.rest.resources;

import com.atlassian.upm.rest.representations.ProductVersionRepresentation;
import com.atlassian.upm.test.UpmResourceTestBase;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

public class ProductVersionResourceIntegrationTest extends UpmResourceTestBase
{
    @Test
    public void assertThatProductVersionIsNotUnknownVersion()
    {
        ProductVersionRepresentation version = restTester.getProductVersionRepresentation();
        assertFalse(version.isUnknown());
    }
    
    @Test
    public void assertThatProductVersionIsNotDevelopmentVersion()
    {
        ProductVersionRepresentation version = restTester.getProductVersionRepresentation();
        assertFalse(version.isDevelopment());
    }
}
