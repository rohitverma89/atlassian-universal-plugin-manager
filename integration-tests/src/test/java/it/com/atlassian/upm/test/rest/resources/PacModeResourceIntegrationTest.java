package it.com.atlassian.upm.test.rest.resources;

import com.atlassian.upm.test.UpmResourceTestBase;
import com.atlassian.upm.test.UpmTestRunner;
import com.atlassian.upm.test.UpmUiTester;
import com.google.inject.Inject;
import com.sun.jersey.api.client.ClientResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.URISyntaxException;

import static com.google.common.base.Preconditions.checkState;
import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(UpmTestRunner.class)
public class PacModeResourceIntegrationTest extends UpmResourceTestBase
{
    @Inject private static UpmUiTester uiTester;

    @BeforeClass
    public static void logIn()
    {
        uiTester.logInAs("admin");
    }

    @Before
    public void goToUpmPage()
    {
        uiTester.goToUpmPage();
    }

    @After
    public void resetPacMode()
    {
        restTester.setPacMode(false);
    }

    @Test
    public void verifySettingBuildNumberGetsExpectedValue() throws URISyntaxException
    {
        ClientResponse response = restTester.setPacMode(true);
        checkState(response.getResponseStatus().equals(OK));
        Boolean pacMode = restTester.getPacMode();
        assertThat(pacMode, is(equalTo(Boolean.TRUE)));
    }
}
