package com.atlassian.upm.test;

import java.util.List;

import com.atlassian.upm.rest.representations.AbstractInstallablePluginCollectionRepresentation;
import com.atlassian.upm.rest.representations.AbstractInstallablePluginCollectionRepresentation.AvailablePluginEntry;
import com.atlassian.upm.rest.representations.ErrorRepresentation;
import com.atlassian.upm.rest.representations.InstalledPluginCollectionRepresentation.PluginEntry;
import com.atlassian.upm.rest.representations.ProductUpgradesRepresentation;
import com.atlassian.upm.rest.resources.upgradeall.UpgradeFailed;
import com.atlassian.upm.rest.resources.upgradeall.UpgradeSucceeded;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.sun.syndication.feed.atom.Entry;
import com.sun.syndication.feed.atom.Feed;
import com.sun.syndication.feed.atom.Link;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import static com.google.common.collect.Iterables.transform;
import static it.com.atlassian.upm.rest.resources.AvailablePlugins.parseKeyFromPath;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasItems;

public final class UpmIntegrationMatchers
{
    private UpmIntegrationMatchers()
    {
        throw new RuntimeException("Cannot instantiate this class");
    }

    @SuppressWarnings("unchecked")
    public static Matcher<Iterable<PluginEntry>> hasPlugins(Matcher<? super PluginEntry> matcher)
    {
        return hasItems(matcher);
    }

    public static Matcher<Iterable<PluginEntry>> hasPlugins(Matcher<? super PluginEntry>... matchers)
    {
        return hasItems(matchers);
    }

    public static Matcher<ErrorRepresentation> hasSubCode(final String subCode)
    {
        return new TypeSafeDiagnosingMatcher<ErrorRepresentation>()
        {
            protected boolean matchesSafely(ErrorRepresentation errorRep, Description mismatchDescription)
            {
                if (!errorRep.getSubCode().equals(subCode))
                {
                    mismatchDescription.appendText("with subCode " + subCode);
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendText("with subCode" + subCode);
            }
        };
    }

    public static Matcher<PluginEntry> systemPlugin(final String pluginKey)
    {
        return new TypeSafeDiagnosingMatcher<PluginEntry>()
        {
            @Override
            protected boolean matchesSafely(PluginEntry entry, Description mismatchDescription)
            {
                if (!pluginKey.equals(getPluginKey(entry)) || entry.isUserInstalled())
                {
                    mismatchDescription.appendText("pluginKey=" + getPluginKey(entry) + ";userInstalled=" + entry.isUserInstalled());
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendText("pluginKey=" + pluginKey + ";system=true");
            }
        };
    }

    public static Matcher<PluginEntry> userInstalledPlugin(TestPlugins plugin, Matcher<PluginEntry> matcher)
    {
        return userInstalledPlugin(plugin.getKey(), matcher);
    }

    public static Matcher<PluginEntry> userInstalledPlugin(TestPlugins plugin, Matcher<PluginEntry>... matchers)
    {
        return userInstalledPlugin(plugin.getKey(), matchers);
    }

    public static Matcher<PluginEntry> userInstalledPlugin(final String pluginKey, Matcher<PluginEntry>... matchers)
    {
        return new TypeSafeDiagnosingMatcher<PluginEntry>()
        {
            @Override
            protected boolean matchesSafely(PluginEntry entry, Description mismatchDescription)
            {
                if (!pluginKey.equals(getPluginKey(entry)) || !entry.isUserInstalled())
                {
                    mismatchDescription.appendText("pluginKey=" + getPluginKey(entry) + ";userInstalled=" + entry.isUserInstalled());
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendText("pluginKey=" + pluginKey + ";userInstalled=true");
            }
        };
    }

    public static Matcher<PluginEntry> withRestartState(final String restartState)
    {
        return new TypeSafeDiagnosingMatcher<PluginEntry>()
        {
            @Override
            protected boolean matchesSafely(PluginEntry plugin, Description mismatchDescription)
            {
                if (!plugin.getRestartState().equals(restartState))
                {
                    mismatchDescription.appendText("with restart state ").appendValue(restartState);
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendText("with restart state ").appendValue(restartState);
            }
        };
    }

    public static Matcher<Feed> feedWithEntriesContaining(TestPlugins plugin)
    {
        return new FeedWithEntriesContainingTextMatcher(plugin.getKey());
    }

    public static Matcher<Feed> feedWithLinkRelAndStartIndex(String relText, int expectedStartIndex)
    {
        return new FeedWithLinkRelAndStartIndexMatcher(relText, expectedStartIndex);
    }

    public static Matcher<Feed> feedWithNonexistantLinkRel(String relText)
    {
        return new FeedWithNonexistantLinkRelMatcher(relText);
    }

    private static final class FeedWithEntriesContainingTextMatcher extends TypeSafeDiagnosingMatcher<Feed>
    {
        private final String text;

        public FeedWithEntriesContainingTextMatcher(String text)
        {
            this.text = text;
        }

        protected boolean matchesSafely(Feed item, Description mismatchDescription)
        {
            for (Entry entry : (List<Entry>) item.getEntries())
            {
                if (!entry.toString().contains(text))
                {
                    mismatchDescription.appendText("was ").appendValueList("[", ", ", "]", item.getEntries());
                    return false;
                }
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("atom feed with entries containing ").appendValue(text);
        }
    }

    private static final class FeedWithLinkRelAndStartIndexMatcher extends TypeSafeDiagnosingMatcher<Feed>
    {
        private final String text;
        private final int startIndex;

        public FeedWithLinkRelAndStartIndexMatcher(String text, int startIndex)
        {
            this.text = text;
            this.startIndex = startIndex;
        }

        protected boolean matchesSafely(Feed item, Description mismatchDescription)
        {
            for (Link link : (List<Link>) item.getOtherLinks())
            {
                if (link.getRel().equals(text))
                {
                    if (link.getHref().contains("start-index=" + startIndex))
                    {
                        mismatchDescription.appendText("contains start-index ").appendValue(startIndex);
                        return true;
                    }
                    else
                    {
                        mismatchDescription.appendText("does not contain start-index ").appendValue(startIndex);
                        return false;
                    }
                }
            }
            return false;
        }

        public void describeTo(Description description)
        {
            description.appendText("atom feed with link rels containing ").appendValue(text).appendText(" and start-index value ").appendValue(startIndex);
        }
    }

    private static final class FeedWithNonexistantLinkRelMatcher extends TypeSafeDiagnosingMatcher<Feed>
    {
        private final String text;

        public FeedWithNonexistantLinkRelMatcher(String text)
        {
            this.text = text;
        }

        protected boolean matchesSafely(Feed item, Description mismatchDescription)
        {
            for (Link link : (List<Link>) item.getOtherLinks())
            {
                if (link.getRel().equals(text))
                {
                    mismatchDescription.appendText("contains rel ").appendValue(text);
                    return false;
                }
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("atom feed without link rels containing ").appendValue(text);
        }
    }

    private static final Function<TestPlugins, String> testPluginsToKeys = new Function<TestPlugins, String>()
    {
        public String apply(TestPlugins from)
        {
            return from.getEscapedKey();
        }
    };

    public static Matcher<? super Iterable<AbstractInstallablePluginCollectionRepresentation.AvailablePluginEntry>> containsPlugins(TestPlugins... plugins)
    {
        return containsPlugins(transform(asList(plugins), testPluginsToKeys));
    }

    private static final Function<String, Matcher<? super AvailablePluginEntry>> keyToPluginEntryMatcher = new Function<String, Matcher<? super AvailablePluginEntry>>()
    {
        public Matcher<? super AvailablePluginEntry> apply(String key)
        {
            return pluginWithKey(key);
        }
    };

    public static Matcher<? super Iterable<AvailablePluginEntry>> containsPlugins(Iterable<String> keys)
    {
        List<Matcher<? super AvailablePluginEntry>> transform = ImmutableList.copyOf(transform(keys, keyToPluginEntryMatcher));
        return contains(transform);
    }

    public static Matcher<? super AbstractInstallablePluginCollectionRepresentation.AvailablePluginEntry> pluginWithKey(String pluginKey)
    {
        return new AvailablePluginEntryPluginKeyMatcher(pluginKey);
    }

    private final static class AvailablePluginEntryPluginKeyMatcher extends TypeSafeDiagnosingMatcher<AbstractInstallablePluginCollectionRepresentation.AvailablePluginEntry>
    {
        private final String pluginKey;

        public AvailablePluginEntryPluginKeyMatcher(String pluginKey)
        {
            this.pluginKey = pluginKey;
        }

        @Override
        protected boolean matchesSafely(AbstractInstallablePluginCollectionRepresentation.AvailablePluginEntry entry, Description mismatchDescription)
        {
            String key = parseKeyFromPath(entry.getSelf().getPath());
            if (!key.equals(pluginKey))
            {
                mismatchDescription.appendText("was plugin with key ").appendText(key);
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("plugin with key ").appendValue(pluginKey);
        }
    }

    public static Matcher<Iterable<ProductUpgradesRepresentation.ProductUpgradeEntry>> containsVersions(String... versionNumbers)
    {
        ImmutableList.Builder<Matcher<? super ProductUpgradesRepresentation.ProductUpgradeEntry>> builder = ImmutableList.builder();
        for (String versionNumber : versionNumbers)
        {
            builder.add(productWithVersionNumber(versionNumber));
        }
        return contains(builder.build());
    }

    public static Matcher<ProductUpgradesRepresentation.ProductUpgradeEntry> productWithVersionNumber(String versionNumber)
    {
        return new ProductUpgradeVersionNumberMatcher(versionNumber);
    }

    private final static class ProductUpgradeVersionNumberMatcher extends TypeSafeDiagnosingMatcher<ProductUpgradesRepresentation.ProductUpgradeEntry>
    {
        private final String versionNumber;

        public ProductUpgradeVersionNumberMatcher(String versionNumber)
        {
            this.versionNumber = versionNumber;
        }

        @Override
        protected boolean matchesSafely(ProductUpgradesRepresentation.ProductUpgradeEntry entry, Description mismatchDescription)
        {
            String entryVersionNumber = entry.getVersion();
            if (!entryVersionNumber.equals(this.versionNumber))
            {
                mismatchDescription.appendText("was product with version number ").appendText(entryVersionNumber);
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("product with version number  ").appendValue(versionNumber);
        }
    }

    public static UpgradeSucceededMatcher upgradeSucceededFor(final String name, final String version)
    {
        return new UpgradeSucceededMatcher(name, version);
    }

    public static UpgradeFailedMatcher downloadFailedFor(final String name, final String version)
    {
        return new DownloadFailedUpgradeMatcher(version, name);
    }

    public static UpgradeFailedMatcher upgradeFailureOf(final String name, final String version)
    {
        return new UpgradeFailureMatcher(name, version);

    }

    public static interface UpgradeFailedMatcher extends Matcher<UpgradeFailed>
    {
    }

    private static final class UpgradeFailureMatcher extends TypeSafeDiagnosingMatcher<UpgradeFailed> implements UpgradeFailedMatcher
    {
        private final String name;
        private final String version;

        private UpgradeFailureMatcher(String name, String version)
        {
            this.name = name;
            this.version = version;
        }

        @Override
        protected boolean matchesSafely(UpgradeFailed item, Description mismatchDescription)
        {
            if (!item.getType().equals(UpgradeFailed.Type.INSTALL))
            {
                mismatchDescription.appendText("is a ").appendValue(item.getType().toString().toLowerCase()).appendText(" failure");
                return false;
            }

            boolean matches = true;
            if (!name.equals(item.getName()) || !version.equals(item.getVersion()))
            {
                matches = false;
                mismatchDescription.appendText("upgrade failure for ").appendValue(item.getName())
                    .appendText(" version ").appendValue(item.getVersion());
            }
            return matches;
        }

        public void describeTo(Description description)
        {
            description.appendText("upgrade failure for ").appendValue(name)
                .appendText(" version ").appendValue(version);
        }
    }

    private static final class DownloadFailedUpgradeMatcher extends TypeSafeDiagnosingMatcher<UpgradeFailed> implements UpgradeFailedMatcher
    {
        private final String version;
        private final String name;

        private DownloadFailedUpgradeMatcher(String version, String name)
        {
            this.version = version;
            this.name = name;
        }

        @Override
        protected boolean matchesSafely(UpgradeFailed item, Description mismatchDescription)
        {
            if (!item.getType().equals(UpgradeFailed.Type.DOWNLOAD))
            {
                mismatchDescription.appendText("is a ").appendValue(item.getType().toString().toLowerCase()).appendText(" failure");
                return false;
            }

            boolean matches = true;
            if (!name.equals(item.getName()) || !version.equals(item.getVersion()))
            {
                matches = false;
                mismatchDescription.appendText("download failure for ").appendValue(item.getName())
                    .appendText(" version ").appendValue(item.getVersion());
            }
            return matches;
        }

        public void describeTo(Description description)
        {
            description.appendText("download failure for ").appendValue(name)
                .appendText(" version ").appendValue(version);
        }
    }

    public static final class UpgradeSucceededMatcher extends TypeSafeDiagnosingMatcher<UpgradeSucceeded>
    {
        private final String version;
        private final String name;

        private UpgradeSucceededMatcher(String name, String version)
        {
            this.version = version;
            this.name = name;
        }

        @Override
        protected boolean matchesSafely(UpgradeSucceeded item, Description mismatchDescription)
        {
            if (item.getName().equals(name) && item.getVersion().equals(version))
            {
                return true;
            }
            mismatchDescription.appendText("upgrade succeeded for plugin ").appendText(name)
                .appendText(" version ").appendValue(version);
            return false;
        }

        public void describeTo(Description description)
        {
            description.appendText("upgrade succeeded for ").appendValue(name)
                .appendText(" version ").appendValue(version);
        }
    }

    private static String getPluginKey(PluginEntry entry)
    {
        String pathInfo = entry.getSelfLink().getPath();
        return parseKeyFromPath(pathInfo);
    }
}
