package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.Suppliers.findElement;
import static com.atlassian.upm.pageobjects.WebElements.MESSAGE;
import static com.google.common.base.Preconditions.checkNotNull;

public class AvailablePluginDetails
{
    private static final By DESCRIPTION = By.className("upm-plugin-description");
    private static final By INSTALL = By.className("upm-install");

    private @Inject AtlassianWebDriver driver;

    private final AvailablePlugin plugin;
    private final Supplier<WebElement> pluginDetails;
    private final Supplier<WebElement> description;
    private final Supplier<WebElement> installButton;

    public AvailablePluginDetails(Supplier<WebElement> pluginDetails, AvailablePlugin plugin)
    {
        this.pluginDetails = checkNotNull(pluginDetails, "pluginDetails");
        this.plugin = checkNotNull(plugin, "plugin");
        this.description = findElement(DESCRIPTION, pluginDetails);
        this.installButton = findElement(INSTALL, pluginDetails);
    }

    public boolean isErrorMessage()
    {
        return pluginDetails.get().getAttribute("class").contains("error");
    }

    public WebElement getDescription()
    {
        return description.get();
    }

    public WebElement getInstallButton()
    {
        return installButton.get();
    }

    public AvailablePluginDetails install()
    {
        getInstallButton().click();
        driver.waitUntilElementIsVisibleAt(MESSAGE, plugin.getWebElement().get());
        return this;
    }

    public WebElement getMessage()
    {
        return plugin.getWebElement().get().findElement(MESSAGE);
    }
}
