package com.atlassian.upm.pageobjects;

import javax.inject.Inject;

import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static com.atlassian.upm.pageobjects.Suppliers.*;

public class PluginModule
{
    private @Inject AtlassianWebDriver driver;

    private static final By ACTIONS = By.className("upm-module-actions");
    
    private static final String CANNOT_DISABLE_CLASSNAME = "upm-module-cannot-disable"; 
    
    private final Supplier<WebElement> actions;

    public PluginModule(Supplier<WebElement> module)
    {
        this.actions = findElement(ACTIONS, module);
    }

    // The CANNOT_DISABLE_CLASSNAME is present for modules that cannot ever be disabled, but not for modules that only
    // temporarily (because someone else is running a long-running task) cannot be disabled
    public boolean canBeDisabled()
    {
        return !actions.get().getAttribute("class").contains(CANNOT_DISABLE_CLASSNAME);
    }

    // Creating this method because we cannot use canBeDisabled() in some cases, as described in its comment above
    public boolean isDisableButtonVisible()
    {
        return driver.elementIsVisibleAt(By.cssSelector(".upm-module-disable"), actions.get());
    }
}
