var upmUpgradeNotification = (function() {

    /**
     * These vars are populated by UpgradeNotificationTransformer, when this web resource is loaded.
     */
    var productId,
            productVersion,
            upmUri,
            upgradeCount,
            title = AJS.format('upm.upgrade.notification.title'),
            body = AJS.format('upm.upgrade.notification.body', upgradeCount, upmUri),
            bodySingular = AJS.format('upm.upgrade.notification.body.singular', upmUri),
            bodyDismiss =  AJS.format('upm.upgrade.notification.body.dismiss'),
            messageContainerId = 'upgrade-notification-message-bar',
            cookieId = 'upm.upgrade.notification.' + productId + '.ignore.count',
            previouslyIgnoredUpgradeCount = AJS.Cookie.read(cookieId);

    /**
     * Dismisses the plugin upgrade notification, saving the current count of upgrades available.
     */
    function dismiss() {
        AJS.$('#' + messageContainerId).hide();
        AJS.Cookie.save(cookieId, upgradeCount);
    }

    /**
     * Chooses the appropriate body text for the number of plugin upgrades available (UPM-1145).
     */
    function getBody() {
        return (upgradeCount == 1) ? bodySingular : body;
    }

    /**
     * Displays the plugin upgrade notification in all JIRA 4.3.x admin screens.
     */
    function notifyJira43x() {
        if (window.location.href.substr(0, upmUri.length) == upmUri) {
            return;
        }

        var container = AJS.$('body > table > tbody > tr > td:nth-child(3)'),
                messageContainer = AJS.$('<div/>').attr('id', messageContainerId).attr('style', 'margin: 30px');
        if (container.length) {
            container.prepend(messageContainer);
            AJS.messages.info('#' + messageContainerId,
                {
                    'title': title,
                    'body': getBody()
                });
            messageContainer.bind('messageClose', dismiss);
        }
    }

    /**
     * Displays the plugin upgrade notification in the "Plugins" section of the main JIRA 4.4.x admin screen.
     */
    function notifyJira44x() {
        var container = AJS.$('#admin-summary-section-admin_plugins_menu'),
                messageContainer = AJS.$('<div/>').attr('id', messageContainerId).attr('style', 'margin: 5px 30px');
        if (container.length) {
            container.prepend(messageContainer);
            AJS.messages.info('#' + messageContainerId,
                {
                    'title': title,
                    'body': getBody()
                });
            messageContainer.bind('messageClose', dismiss);
        }
    }

    /**
     * Displays the plugin upgrade notification on the main Confluence admin screen.
     */
    function notifyConfluence() {
        if (window.location.href.indexOf('console.action') == -1 &&
            window.location.href.indexOf('editconsolemessages.action') == -1) {
            return;
        }

        var blurb = AJS.$('td.pagebody > p:first'),
                messageContainer = AJS.$('<div/>').attr('id', messageContainerId),
                dismissLinkId = 'dismiss-upgrade-notification',
                dismissLinkText = AJS.format('(<a href="#" id="{0}">{1}</a>)', dismissLinkId, bodyDismiss);
        if (blurb.length) {
            blurb.after(messageContainer);
            AJS.messages.info('#' + messageContainerId,
                {
                    'title': title,
                    'body': getBody() + ' ' + dismissLinkText,
                    'shadowed': false,
                    'closeable': false
                });
            AJS.$('#' + dismissLinkId).click(dismiss);
        }
    }

    function notify() {
        if (productId == 'jira') {
            switch (productVersion.substr(0, 3)) {
                case '4.3':
                    notifyJira43x();
                    break;
                case '4.4':
                    notifyJira44x();
                    break;
            }
        } else if (productId == 'confluence') {
            notifyConfluence();
        }
    }

    AJS.toInit(function() {
        // if upgrade count is missing, something's broken; fail silently.
        if (upgradeCount == undefined) {
            return;
        }

        // if the current upgrade count is less than the previously ignored count,
        // reset the count to the lower value and continue suppressing the message.
        if (previouslyIgnoredUpgradeCount && upgradeCount <= previouslyIgnoredUpgradeCount) {
            AJS.Cookie.save(cookieId, upgradeCount);
            return;
        }

        // otherwise, erase the previously ignored count and show the message.
        AJS.Cookie.erase(cookieId);
        if (upgradeCount > 0) {
            notify();
        }
    });

})();