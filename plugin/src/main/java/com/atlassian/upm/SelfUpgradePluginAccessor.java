package com.atlassian.upm;

import java.io.File;
import java.net.URI;

/**
 * Interface used by {@link SelfUpgradeController} to abstract away references to a
 * dynamically loaded component.
 */
public interface SelfUpgradePluginAccessor
{
    URI prepareUpgrade(File jarToInstall, String expectedPluginKey, URI pluginUri, URI selfUpgradePluginUri);
}
