package com.atlassian.upm.rest.representations;

import java.net.URI;
import java.text.Collator;
import java.util.Collection;
import java.util.Locale;
import java.util.Map;

import com.atlassian.upm.PluginAccessorAndController;
import com.atlassian.upm.rest.UpmUriBuilder;
import com.atlassian.upm.spi.Plugin;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Ordering;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.upm.spi.Permission.GET_AUDIT_LOG;
import static com.atlassian.upm.spi.Permission.GET_PRODUCT_UPGRADE_COMPATIBILITY;
import static com.atlassian.upm.spi.Permission.MANAGE_AUDIT_LOG;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_ENABLEMENT;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_INSTALL;
import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_UNINSTALL;
import static com.google.common.collect.Iterables.transform;

/**
 * Jackson representation of the plugins installed in the current configuration.
 */
public class InstalledPluginCollectionRepresentation
{
    @JsonProperty final Collection<PluginEntry> plugins;
    @JsonProperty final Map<String, URI> links;
    @JsonProperty final boolean safeMode;

    @JsonCreator
    public InstalledPluginCollectionRepresentation(@JsonProperty("plugins") Collection<PluginEntry> plugins,
        @JsonProperty("links") Map<String, URI> links,
        @JsonProperty("safeMode") boolean safeMode)
    {
        this.plugins = ImmutableList.copyOf(plugins);
        this.links = ImmutableMap.copyOf(links);
        this.safeMode = safeMode;
    }

    InstalledPluginCollectionRepresentation(PluginAccessorAndController pluginAccessorAndController, UpmUriBuilder uriBuilder, LinkBuilder linkBuilder, Locale locale)
    {
        safeMode = pluginAccessorAndController.isSafeMode();
        links = linkBuilder.buildLinksFor(uriBuilder.buildInstalledPluginCollectionUri())
            .putIfPermitted(GET_AUDIT_LOG, "audit-log", uriBuilder.buildAuditLogFeedUri())
            .putIfPermitted(GET_AUDIT_LOG, "audit-log-max-entries", uriBuilder.buildAuditLogMaxEntriesUri())
            .putIfPermitted(GET_AUDIT_LOG, "audit-log-purge-after", uriBuilder.buildAuditLogPurgeAfterUri())
            .putIfPermitted(MANAGE_AUDIT_LOG, "audit-log-purge-after-manage", uriBuilder.buildAuditLogPurgeAfterUri())
            .putIfPermitted(GET_PRODUCT_UPGRADE_COMPATIBILITY, "product-upgrades", uriBuilder.buildProductUpgradesUri())
            .putIfPermitted(MANAGE_PLUGIN_INSTALL, "install", uriBuilder.buildInstalledPluginCollectionUri())
            .build();
        plugins = ImmutableList.copyOf(transform(
            new PluginOrdering(locale).sortedCopy(pluginAccessorAndController.getPlugins()),
            new PluginToEntryFunction(pluginAccessorAndController, uriBuilder, linkBuilder)));
    }

    public Iterable<PluginEntry> getPlugins()
    {
        return this.plugins;
    }

    public Map<String, URI> getLinks()
    {
        return links;
    }

    public boolean isSafeMode()
    {
        return safeMode;
    }

    public static final class PluginEntry
    {
        @JsonProperty private final boolean enabled;
        @JsonProperty private final Map<String, URI> links;
        @JsonProperty private final String name;
        @JsonProperty private final String key;
        @JsonProperty private final boolean userInstalled;
        @JsonProperty("static") private final boolean staticPlugin;
        @JsonProperty private final String restartState;
        @JsonProperty private final String description;

        @JsonCreator
        public PluginEntry(@JsonProperty("enabled") boolean enabled,
            @JsonProperty("links") Map<String, URI> links,
            @JsonProperty("name") String name,
            @JsonProperty("userInstalled") boolean userInstalled,
            @JsonProperty("static") boolean staticPlugin,
            @JsonProperty("restartState") String restartState,
            @JsonProperty("description") String description,
            @JsonProperty("key") String key)
        {
            this.enabled = enabled;
            this.links = ImmutableMap.copyOf(links);
            this.name = name;
            this.userInstalled = userInstalled;
            this.staticPlugin = staticPlugin;
            this.restartState = restartState;
            this.description = description;
            this.key = key;
        }

        PluginEntry(Plugin plugin,
            PluginAccessorAndController pluginAccessorAndController,
            UpmUriBuilder uriBuilder,
            LinkBuilder linkBuilder)
        {
            this.enabled = pluginAccessorAndController.isPluginEnabled(plugin.getKey());
            this.links = linkBuilder.buildLinkForSelf(uriBuilder.buildPluginUri(plugin.getKey()))
                .putIfPermitted(MANAGE_PLUGIN_ENABLEMENT, plugin, "modify", uriBuilder.buildPluginUri(plugin.getKey()))
                .putIfPermitted(MANAGE_PLUGIN_UNINSTALL, plugin, "delete", uriBuilder.buildPluginUri(plugin.getKey()))
                .build();
            this.userInstalled = pluginAccessorAndController.isUserInstalled(plugin);
            this.staticPlugin = plugin.isStaticPlugin();
            this.restartState = RestartState.toString(pluginAccessorAndController.getRestartState(plugin));
            this.description = plugin.getPluginInformation().getDescription();
            this.name = plugin.getName();
            this.key = plugin.getKey();
        }

        public URI getSelfLink()
        {
            return links.get("self");
        }

        public boolean isUserInstalled()
        {
            return this.userInstalled;
        }

        public boolean isEnabled()
        {
            return this.enabled;
        }

        public boolean isStatic()
        {
            return this.staticPlugin;
        }

        public String getName()
        {
            return name;
        }

        public String getRestartState()
        {
            return restartState;
        }

        public String getDescription()
        {
            return description;
        }

        public String getKey()
        {
            return key;
        }
    }

    private static final class PluginOrdering extends Ordering<Plugin>
    {
        private final Collator collator;

        public PluginOrdering(Locale locale)
        {
            collator = Collator.getInstance(locale);
        }

        public int compare(Plugin p1, Plugin p2)
        {
            int result = collator.compare(getNameOrKey(p1), getNameOrKey(p2));
            return result != 0 ? result : collator.compare(p1.getKey(), p2.getKey());
        }

        private static String getNameOrKey(Plugin p)
        {
            String name = p.getName();
            return name != null ? name : p.getKey();
        }
    }

    private static final class PluginToEntryFunction implements Function<Plugin, PluginEntry>
    {
        private final PluginAccessorAndController pluginAccessorAndController;
        private final UpmUriBuilder uriBuilder;
        private final LinkBuilder linkBuilder;

        public PluginToEntryFunction(PluginAccessorAndController pluginAccessorAndController, UpmUriBuilder uriBuilder, LinkBuilder linkBuilder)
        {
            this.pluginAccessorAndController = pluginAccessorAndController;
            this.uriBuilder = uriBuilder;
            this.linkBuilder = linkBuilder;
        }

        public PluginEntry apply(Plugin plugin)
        {
            return new PluginEntry(plugin, pluginAccessorAndController, uriBuilder, linkBuilder);
        }
    }

}
