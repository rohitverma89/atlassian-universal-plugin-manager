package com.atlassian.upm.rest.representations;

import java.net.URI;
import java.util.Collection;
import java.util.Map;

import com.atlassian.plugins.domain.model.plugin.PluginVersion;
import com.atlassian.upm.PluginAccessorAndController;
import com.atlassian.upm.rest.UpmUriBuilder;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.upm.spi.Permission.MANAGE_PLUGIN_INSTALL;

public class UpgradesRepresentation extends AbstractInstallablePluginCollectionRepresentation
{
    @JsonCreator
    public UpgradesRepresentation(
            @JsonProperty("links") Map<String, URI> links,
            @JsonProperty("plugins") Collection<AvailablePluginEntry> plugins,
            @JsonProperty("safeMode") boolean safeMode)
    {
        super(links, plugins, safeMode);
    }

    public UpgradesRepresentation(Iterable<PluginVersion> upgrades, UpmUriBuilder uriBuilder,
                                  PluginAccessorAndController pluginAccessorAndController, LinkBuilder linkBuilder)
    {
        super(buildLinks(linkBuilder, uriBuilder), upgrades, uriBuilder, pluginAccessorAndController);
    }

    private static Map<String, URI> buildLinks(LinkBuilder linkBuilder, UpmUriBuilder uriBuilder)
    {
        return linkBuilder.buildLinksFor(uriBuilder.buildUpgradesUri())
            .putIfPermitted(MANAGE_PLUGIN_INSTALL, "upgrade-all", uriBuilder.buildUpgradeAllUri())
            .build();
    }
}
