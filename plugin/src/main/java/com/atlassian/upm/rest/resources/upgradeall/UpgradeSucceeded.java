package com.atlassian.upm.rest.resources.upgradeall;

import com.atlassian.plugins.domain.model.plugin.PluginVersion;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public final class UpgradeSucceeded
{
    @JsonProperty private final String name;
    @JsonProperty private final String version;

    @JsonCreator
    public UpgradeSucceeded(@JsonProperty("name") String name, @JsonProperty("version") String version)
    {
        this.name = name;
        this.version = version;
    }

    UpgradeSucceeded(PluginVersion upgrade)
    {
        this(upgrade.getPlugin().getName(), upgrade.getVersion());
    }

    public String getName()
    {
        return name;
    }

    public String getVersion()
    {
        return version;
    }
}